package valDelegates

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

interface ValDelegate<T>: ReadOnlyProperty<Any?, T> {
    val value: T

    fun onGet(valueGot: T)

    override operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        onGet(value)
        return value
    }
}
