import async.AsynchronousDelegate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import lateinit.SingleSetVar
import list.BaseImmutableListDelegateClass
import valDelegates.ValDelegate
import varDelegates.VarDelegate
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

inline fun <T> valDelegate(
    initialValue: T,
    crossinline doOnGet: (T) -> Unit
): ReadOnlyProperty<Any?, T> =
    object : ValDelegate<T> {
        override val value: T = initialValue
        override fun onGet(valueGot: T): Unit = doOnGet(valueGot)
    }

fun <T> singleSetVar(
    doOnSet: (T) -> Unit,
    defaultValue: T? = null,
    doOnGet: (T) -> Unit
): ReadWriteProperty<Any?, T> =
    SingleSetVar(onSet = doOnSet, defaultValue = defaultValue, onGet = doOnGet)

inline fun <T> varDelegate(
    initialValue: T,
    crossinline doBefore: (KProperty<*>, T, T) -> Unit = { prop, old, new ->},
    crossinline passCondition: (T, T) -> Boolean = {old, new -> true },
    crossinline doOnFail: (KProperty<*>, T, T) -> Unit = { prop, old, new ->},
    crossinline doAfterChange: (KProperty<*>, T, T) -> Unit = { prop, old, new ->},
    crossinline doOnChange: (T) -> Unit = { }
): ReadWriteProperty<Any?, T> =
    object: VarDelegate<T> {
        override var value: T = initialValue
        override fun preInvoke(property: KProperty<*>, oldValue: T, newValue: T) = doBefore(property, oldValue, newValue)
        override fun passCondition(oldValue: T, newValue: T): Boolean = passCondition(oldValue, newValue)
        override fun onFail(property: KProperty<*>, oldValue: T, newValue: T) = doOnFail(property, oldValue, newValue)
        override fun afterChange(property: KProperty<*>, oldValue: T, newValue: T) = doAfterChange(property, oldValue, newValue)
        override fun onGet(valueGot: T) = doOnChange(valueGot)
    }

fun <T> asyncVal(scope: CoroutineScope = GlobalScope, initializer: suspend () -> T): ReadOnlyProperty<Any?, T> =
    AsynchronousDelegate(scope, initializer)

fun <T> immutableListDelegate(
    values: List<T>,
    doOnGet: (List<T>) -> Unit = {},
    doOnGetItem: (Int, T) -> Unit
): ReadOnlyProperty<Any?, List<T>> =
    BaseImmutableListDelegateClass(values, doOnGet, doOnGetItem)
