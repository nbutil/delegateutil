package async

import kotlinx.coroutines.*
import kotlin.reflect.KProperty
import kotlin.properties.ReadOnlyProperty

class AsynchronousDelegate<T>(coroScope: CoroutineScope, initializer: suspend () -> T): CoroutineScope by coroScope, ReadOnlyProperty<Any?, T> {
    private val value = async { initializer() }
    override operator fun getValue(thisRef: Any?, property: KProperty<*>) = runBlocking { value.await() }
}