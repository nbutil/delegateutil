package varDelegates

import valDelegates.ValDelegate
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

interface VarDelegate<T>: ValDelegate<T>, ReadWriteProperty<Any?, T> {

    override var value: T

    fun preInvoke(property: KProperty<*>, oldValue: T, newValue: T)

    fun passCondition(oldValue: T, newValue: T): Boolean

    fun onFail(property: KProperty<*>, oldValue: T, newValue: T)

    fun afterChange(property: KProperty<*>, oldValue: T, newValue: T)

    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        onGet(this.value)
        return this.value
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        val oldValue = this.value
        preInvoke(property, oldValue, value)
        if (oldValue == value) {
            afterChange(property, oldValue, value)
            return
        }
        if (!passCondition(oldValue, value)) {
            onFail(property, oldValue, value)
            return
        }
        this.value = value
        afterChange(property, oldValue, value)
    }
}
