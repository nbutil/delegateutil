package lateinit

internal class DelegateAlreadySet(override val message: String): Exception(message)
internal class DelegateNotSet(override val message: String): Exception(message)