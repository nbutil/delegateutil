package lateinit

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class SingleSetVar<T>(
    private inline val onSet: (T) -> Unit,
    private val defaultValue: T? = null,
    private inline val onGet: (T) -> Unit
): ReadWriteProperty<Any?, T> {

    private var hasDefault: Boolean = defaultValue != null

    var isInitialized: Boolean = false
        private set

    private var value: T? = null

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        if (isInitialized) {
            throw DelegateAlreadySet("This delegate has already been set and cannot be set again")
        }
        onSet(value)
        this.value = value
        this.isInitialized = true
        this.hasDefault = false
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        val value: T
        if (hasDefault) {
            value = this.defaultValue as T
            onGet(value)
            return value
        } else if (isInitialized) {
            value = this.value as T
            onGet(value)
            return value
        }
        throw DelegateNotSet("This lateinit delegate has not been initialized and cannot be gotten")
    }
}
