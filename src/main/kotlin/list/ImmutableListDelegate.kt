package list

import valDelegates.ValDelegate

interface ImmutableListDelegate<T>: ValDelegate<List<T>>, List<T>
