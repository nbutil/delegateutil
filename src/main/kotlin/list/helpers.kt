package list

internal class DelegatedList<T> private constructor (
    private inline val onGetItem: (Int, T) -> Unit,
    private val originalValues: List<T>
): List<T> by originalValues {

    constructor(values: List<T>, onGetItem: (Int, T) -> Unit):
            this(onGetItem, values)


    override operator fun get(index: Int): T {
        val item = originalValues[index]
        onGetItem(index, item)
        return item
    }

    override fun iterator(): Iterator<T> = DelegateIterator(originalValues, onGetItem)

    internal class DelegateIterator<T>(private val toIterateOn: List<T>, private val delegateFunction: (Int, T) -> Unit): Iterator<T> {
        private var currentIndex: Int = 0
        private var lastIndex: Int = 0
        override fun hasNext(): Boolean = currentIndex < toIterateOn.size

        override fun next(): T {
            if (currentIndex >= toIterateOn.size) {
                currentIndex = 0
            }
            lastIndex = currentIndex
            val item = toIterateOn[currentIndex]
            delegateFunction(currentIndex++, item)
            return item
        }
    }


    override fun toString(): String = originalValues.toString()
}

class BaseImmutableListDelegateClass<T> private constructor (
    override val value: List<T>,
    private inline val doOnGet: (List<T>) -> Unit
): ImmutableListDelegate<T>, List<T> by value {

    constructor(values: List<T>, doOnGet: (List<T>) -> Unit, doOnGetItem: (Int, T) -> Unit):
            this(DelegatedList(values.toList(), doOnGetItem), doOnGet)

    override fun onGet(valueGot: List<T>) = doOnGet(valueGot)

}

